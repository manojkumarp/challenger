'use strict';
/**
 * @ngdoc overview
 * @name collaborativeLearning
 * @description
 * # collaborativeLearning
 *
 * Main module of the application.
 */
angular.module('collaborativeLearning')
  .controller('reportcntrl', function ($rootScope,$scope,socket,$http,$state,$stateParams) {
  		console.log("parent scope",$scope.$parent.medium1C);
  		$scope.correct1 = $scope.$parent.easy1C +  $scope.$parent.medium1C + $scope.$parent.hard1C;
  		$scope.incorrect1 = $scope.$parent.easy1CW +  $scope.$parent.medium1CW + $scope.$parent.hard1CW;
  		$scope.correct2= $scope.$parent.easy2C +  $scope.$parent.medium2C + $scope.$parent.hard2C;
  		$scope.incorrect2 = $scope.$parent.easy2CW +  $scope.$parent.medium2CW + $scope.$parent.hard2CW;

	  $scope.chartConfig1 = {
		        options: {
		            chart: {
		                type: 'pie'
		            }
		        },
		        series: [{
		        data: [{
                name: 'Correct',
                y: 	$scope.correct1,
                color: '#4dff4d',
            	fillColor : '#4dff4d',
            	selected: true,
            	slice:true
	            }, {
	                name: 'Wrong',
	                y: $scope.incorrect1,
                	color: '#ff1f17',
            		fillColor : '#ff1f17',	
            		selected: true,
            		slice:true             
	            }]
		        }],
		        title: {
		            text: 'Mine'
		        },

		        loading: false
		    }  		


  $scope.chartConfig2 = {
		        options: {
		            chart: {
		                type: 'pie'
		            }
		        },
		        series: [{
		        data: [{
                name: 'Correct',
                y: 	$scope.correct2,
               	color: '#4dff4d',
            	fillColor : '#4dff4d',
            	selected: true,
            	slice:true               
	            }, {
	                name: 'Wrong',
	                y: $scope.incorrect2,
	                color: '#ff1f17',
            		fillColor : '#ff1f17',
            		selected: true,
            		slice:true    
	            }]
		        }],
		        title: {
		            text: 'Other'
		        },

		        loading: false
		    }  			    
		
});



